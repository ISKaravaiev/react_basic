import { rest } from 'msw'
export const handlers = [
  // Handles a GET /form request
  rest.get('/', null),
  // Handles a POST /admin request
  rest.post('/admin', (req, res, ctx) => {
    // Persist user's authentication in the session
    sessionStorage.setItem('is-authenticated', 'true')
    return res(
      // Respond with a 200 status code
      ctx.status(200),
    )
  }),
]